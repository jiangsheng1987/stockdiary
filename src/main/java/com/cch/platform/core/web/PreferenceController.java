package com.cch.platform.core.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cch.platform.base.bean.BaseUser;
import com.cch.platform.core.service.PreferenceService;


@Controller
@RequestMapping(value = "/core/preference")
public class PreferenceController {

	@Autowired
	PreferenceService ps=null;
	
	@RequestMapping(value = "/get.do")
	public String get(HttpServletRequest request,ModelMap mm) {
		BaseUser user=(BaseUser)request.getSession().getAttribute("user");
		String[] prefCodes=request.getParameter("prefCodes").split(",");
		for(String prefCode:prefCodes){
			String prefValue=ps.getPreference(prefCode, user);
			mm.put(prefCode, prefValue);
		}
		return "jsonView";
	}
	
	@RequestMapping(value = "/saveValue.do")
	public String saveValue(HttpServletRequest request) {
		BaseUser user=(BaseUser)request.getSession().getAttribute("user");
		String prefCode=request.getParameter("prefCode");
		String prefValue=request.getParameter("prefValue");
		ps.saveValue(prefCode,prefValue,user);
		return "jsonView";
	}
	
}
