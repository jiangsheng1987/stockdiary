package com.cch.platform.base.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * BaseRole entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "base_role")
public class BaseRole implements java.io.Serializable {

	// Fields

	private Integer roleId;
	private String roleCode;
	private String roleName;

	// Constructors

	/** default constructor */
	public BaseRole() {
	}

	/** full constructor */
	public BaseRole(Integer roleId, String roleCode, String roleName) {
		this.roleId = roleId;
		this.roleCode = roleCode;
		this.roleName = roleName;
	}

	// Property accessors
	@Id
	@Column(name = "role_id", unique = true, nullable = false)
	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	@Column(name = "role_code", nullable = false, length = 100)
	public String getRoleCode() {
		return this.roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	@Column(name = "role_name", nullable = false, length = 100)
	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}