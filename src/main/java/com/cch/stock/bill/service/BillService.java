package com.cch.stock.bill.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cch.platform.service.BaseService;
import com.cch.platform.util.DateUtil;
import com.cch.platform.util.MapUtil;
import com.cch.platform.util.StringUtil;
import com.cch.platform.web.WebUtil;
import com.cch.stock.bill.bean.SdBill;
import com.cch.stock.bill.bean.SdTradeType;

@Service
public class BillService extends BaseService<SdBill> {
	
	public void saveOrUpdate(SdBill bill) throws Exception {
		bill.setUserId(WebUtil.getUser().getUserId());
		this.dealCatory(bill);
		this.dealMoney(bill);
		
		Timestamp ts= DateUtil.getTimestamp();
		bill.setUpdateTime(ts);
		if(bill.getBillId()==null){
			bill.setCreateTime(ts);
		}
		super.saveOrUpdate(bill);
	}
	
	public void importBstransfer(List<Map<String, Object>> billDatas) throws Exception{
		for(Map<String, Object> billData: billDatas){
			SdBill bill = null;
			
			Map<String, Object> queryMap=new HashMap<String, Object>();
			Integer billSerialNumber= (Integer)billData.get("billSerialNumber");
			queryMap.put("billSerialNumber", billSerialNumber);
			Date billDate=(Date)billData.get("billDate");
			queryMap.put("billDate", billDate);
			List<SdBill> re = this.beanDao.find(SdBill.class, queryMap);
			if (re.size() == 0) {
				bill = new SdBill();
				bill.setBillSerialNumber(billSerialNumber);
				bill.setBillDate(new Timestamp(billDate.getTime()));
			}else if(re.size()==1){
				bill=re.get(0);
			}else {
				throw new Exception("bad data,repeat:"+ billSerialNumber+" "+billDate);
			}
			
			Integer catory = (Integer) billData.get("billCatory");
			bill.setBillBigcatory("");
			double billMoney=(catory==2041)?(Double) billData.get("moneyIn"):-(Double) billData.get("moneyOut");
			bill.setBillMoney(billMoney);
			bill.setBillDesc(String.valueOf(billData.get("billDesc")));
			bill.setUpdateTime(new Timestamp(new Date().getTime()));
			this.beanDao.saveOrUpdate(bill);
		}
	}
	
	private void dealMoney(SdBill bill) {
		if("BSTRANS".equals(bill.getBillBigcatory())) return;
		//allprice=-cnt*(price+inter)
		if(bill.getStockCnt()==null)bill.setStockCnt(0);
		if(bill.getStockPrice()==null)bill.setStockPrice(0.0);
		if(bill.getStockInterest()==null)bill.setStockInterest(0.0);
		double allprice=-bill.getStockCnt()*(bill.getStockPrice()+bill.getStockInterest());
		bill.setStockAllpric(allprice);
		
		if(bill.getFeeCommision()==null)bill.setFeeCommision(0.0);
		if(bill.getFeeTax()==null)bill.setFeeTax(0.0);
		if(bill.getFeeOther()==null)bill.setFeeOther(0.0);
		double fellall=bill.getFeeCommision()+bill.getFeeOther()+bill.getFeeTax();
		bill.setFeeAll(fellall);
		
		bill.setBillMoney(bill.getStockAllpric()-bill.getFeeAll());	
	}

	private void dealCatory(SdBill bill) throws Exception{
		String catory=bill.getBillCatory();
		if(StringUtil.isEmpty(catory)){
			throw new Exception("类别不能为空！");
		}
		SdTradeType type=beanDao.get(SdTradeType.class, catory);
		if(type!=null) bill.setBillBigcatory(type.getTradePcode());
		else throw new Exception("类别不存在！");
	}
}