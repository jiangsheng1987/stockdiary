package com.cch.stock.bill.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.cch.platform.base.bean.BaseUser;
import com.cch.platform.dao.BaseDao;
import com.cch.stock.bill.bean.SdBill;

@Repository
public class BillDao extends BaseDao<SdBill>{

	@SuppressWarnings("rawtypes")
	public Date getFirstBillDate(BaseUser user){
		String hql="select min(billDate) from SdBill where userId=?";
		List ft=this.find(hql,user.getUserId());
		if(ft.get(0)==null) return null;
		return (Date)(ft.get(0));
	}
	
}
